variable cidr_block {}
variable availability_zone {}
variable subnet_cidr_block {}
variable environment_prefix {}
variable my_ip {}
variable instance_type{}
variable public_key{}
variable private_key_location{}
variable image_name {}
