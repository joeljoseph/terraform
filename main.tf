provider "aws" {
    region = "us-east-2"
    access_key = "AKIAXWBUR4CXFDYAVEUZ"
    secret_key = "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"

}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = var.cidr_block

  azs             = [var.availability_zone]
  
  public_subnets  = [var.subnet_cidr_block]
  public_subnet_tags = { Name = "${var.environment_prefix}-subnet-1"}

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    Terraform = "true"
    Name : "${var.environment_prefix}-vpc"
  }
}

module "myapp-server" {
    source = "./modules/webserver"
    vpc_id = module.vpc.vpc_id
    my_ip = var.my_ip
    environment_prefix = var.environment_prefix
    image_name = var.image_name
    instance_type = var.instance_type
    subnet_id = module.vpc.public_subnets[0]
    availability_zone = var.availability_zone
    public_key = var.public_key


}